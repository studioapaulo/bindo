<?php 
function stps($a)// sécurisation
{
    $v=htmlspecialchars($a);
    return $v;
}
function init_solde($idp,$bdd)// initaialisation du solde
{
    $id=stp($idp);
    $verif=$bdd->prepare("SELECT* FROM myaccount WHERE idp=:id");
    $verif->execute(array("id"=>$id));
    if($ver=$verif->fetch())
    {
        
    }else{
        $req=$bdd->prepare("INSERT INTO myaccount (solde_actu,a_payer,a_payer_par_mois,idp) VALUES (:a,:b,:c,:d)");
        $req->execute(array(
            "a"=>0,
            "b"=>0,
            "c"=>0,
            "d"=>$id
        ));
    }
   
}
function solde($idp,$bdd) // recupération du solde
{
    $idu=stps($idp);
    $req=$bdd->prepare("SELECT solde_actu FROM myaccount WHERE idp=:id");
    $req->execute(array(
        "id"=> $idu
    ));
    if($info=$req->fetch())
    {
        return $info['solde_actu'];
    }
}
function update_solde($new_solde,$idp,$bdd)// mise à jour du solde
{
    $nid=stps($idp);
    $solde_n=stps($new_solde);
    $solde_a=stps(solde($idp,$bdd));
    $balance=$solde_a+ $solde_n;
    $reqv=$bdd->prepare('UPDATE myaccount SET solde_actu=:solde WHERE idp=:id');
    $reqv->execute(array(
        "solde"=> $balance,
        "id"=> $nid
    ));
}
function deduct_solde($somme,$idp,$bdd)// deduire uu solde
{
    $id=stps($idp);
    $solde=stps(solde($id,$bdd));
    $sommev=stps($somme);
    $new_balance=$solde-$sommev;
    $req=$bdd->prepare('UPDATE myaccount SET solde_actu=:solde WHERE idp=:id');
    $req->execute(array(
        "solde"=> $new_balance,
        "id"=> $id
    ));

}
function sup_compte($idp,$bdd)// suppression
{
    $prop=stps($idp);
    $casse=$bdd->prepare('DELETE FROM myaccount WHERE idp=:b');
    $casse->execute(array(
                            "b"=>$prop
                       
                    ));
}
function retrait_compte($idp,$bdd,$bdds)// retrait des compte au solde négatif
{
    $solde=solde($idp,$bdds);
    $id=stps($idp);
    if($solde<0)
    {
        $req=$bdd->prepare('UPDATE maison SET m_stat=:st WHERE idp=:id');
        $req->execute(array(
        "st"=> 'bloque',
        "id"=> $id
            ));

    }
}
function retraitp_compte($idp,$bdd,$bdds)// reactivation des comptes au solde négatif vers positif
{
    $solde=solde($idp,$bdds);
    $id=stps($idp);
    if($solde>0)
    {
        $req=$bdd->prepare('UPDATE maison SET m_stat=:st WHERE idp=:id');
        $req->execute(array(
        "st"=> 'autoris',
        "id"=> $id
            ));

    }
}
function mois_compte($idp,$bdd)// a payer par mois
{
    $idu=stps($idp);
    $req=$bdd->prepare("SELECT a_payer_par_mois FROM myaccount WHERE idp=:id");
    $req->execute(array(
        "id"=> $idu
    ));
    if($info=$req->fetch())
    {
        return $info['a_payer_par_mois'];
    }
}
function mod_mois_pay($somme,$idp,$bdd)// mise à jour de la somme à payer par mois
{
    $idu=stps($idp);
    $new_balance=stps($somme);
    $req=$bdd->prepare('UPDATE myaccount SET a_payer_par_mois=:solde WHERE idp=:id');
    $req->execute(array(
        "solde"=> $new_balance,
        "id"=> $idu
    ));

}
function devoir_compte($somme,$idp,$bdd)// Devoir d'un compte
{
    if($somme<0)
    {
        return solde($idp,$bdd)*(-1);
    } else {
        return 0;
    }
}
?>