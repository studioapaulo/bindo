-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 22 Juillet 2019 à 03:09
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bd_apl_lm`
--
CREATE DATABASE IF NOT EXISTS `bd_apl_lm` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_apl_lm`;

-- --------------------------------------------------------

--
-- Structure de la table `admin_conf`
--

CREATE TABLE IF NOT EXISTS `admin_conf` (
  `ida` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `autor` int(10) NOT NULL,
  PRIMARY KEY (`ida`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `admin_conf`
--

INSERT INTO `admin_conf` (`ida`, `email`, `autor`) VALUES
(1, 'suiniolive58@gmail.com', 1),
(3, 'lanlan@gmail.com', 2);

-- --------------------------------------------------------

--
-- Structure de la table `admin_mdp`
--

CREATE TABLE IF NOT EXISTS `admin_mdp` (
  `idm` int(20) NOT NULL AUTO_INCREMENT,
  `mdp` varchar(50) NOT NULL,
  `ida` int(20) NOT NULL,
  PRIMARY KEY (`idm`),
  KEY `ida` (`ida`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `admin_mdp`
--

INSERT INTO `admin_mdp` (`idm`, `mdp`, `ida`) VALUES
(1, 'angededieu2', 1),
(3, 'okokok', 3);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `idcom` int(20) NOT NULL AUTO_INCREMENT,
  `idmaison` int(20) NOT NULL,
  `nom` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `rang` int(10) NOT NULL,
  `commentaire` text NOT NULL,
  `dates` date NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`idcom`),
  KEY `idp` (`idp`),
  KEY `idmaison` (`idmaison`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `favorie`
--

CREATE TABLE IF NOT EXISTS `favorie` (
  `idfa` int(50) NOT NULL AUTO_INCREMENT,
  `idmaison` int(20) NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`idfa`),
  KEY `idmaison` (`idmaison`,`idp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

-- --------------------------------------------------------

--
-- Structure de la table `g_messagerie`
--

CREATE TABLE IF NOT EXISTS `g_messagerie` (
  `idg` int(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `notel` varchar(20) NOT NULL,
  `msg` text NOT NULL,
  `lire` varchar(10) NOT NULL,
  `dates` datetime NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`idg`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Structure de la table `image_m`
--

CREATE TABLE IF NOT EXISTS `image_m` (
  `idim` int(20) NOT NULL AUTO_INCREMENT,
  `idsp` int(20) NOT NULL,
  `img` varchar(150) NOT NULL,
  `idmaison` int(20) NOT NULL,
  PRIMARY KEY (`idim`),
  KEY `idmaison` (`idmaison`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Contenu de la table `image_m`
--

INSERT INTO `image_m` (`idim`, `idsp`, `img`, `idmaison`) VALUES
(13, 0, '5d34fa9648ca8.png', 0),
(14, 0, '5d34fa965e7e8.png', 0),
(15, 0, '5d34fa96754f5.png', 0);

-- --------------------------------------------------------

--
-- Structure de la table `infosup`
--

CREATE TABLE IF NOT EXISTS `infosup` (
  `idinf` int(20) NOT NULL AUTO_INCREMENT,
  `idmaison` varchar(25) NOT NULL,
  `adress` varchar(150) NOT NULL,
  `ville` int(20) NOT NULL,
  `quartier` varchar(150) NOT NULL,
  `voisin` int(10) NOT NULL,
  PRIMARY KEY (`idinf`),
  KEY `idmaison` (`idmaison`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `infosup`
--

INSERT INTO `infosup` (`idinf`, `idmaison`, `adress`, `ville`, `quartier`, `voisin`) VALUES
(12, 'Mais12019', 'vdcd', 0, 'lmvfs', 0),
(13, 'Mais12019', 'Cocody', 0, 'Grande zone', 2);

-- --------------------------------------------------------

--
-- Structure de la table `lien_social`
--

CREATE TABLE IF NOT EXISTS `lien_social` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(200) NOT NULL,
  `twitter` varchar(200) NOT NULL,
  `google` varchar(200) NOT NULL,
  `linkedln` varchar(200) NOT NULL,
  `instagram` varchar(200) NOT NULL,
  `printerest` varchar(200) NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`ids`),
  KEY `idp` (`idp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `lien_social`
--

INSERT INTO `lien_social` (`ids`, `facebook`, `twitter`, `google`, `linkedln`, `instagram`, `printerest`, `idp`) VALUES
(1, '', '', 'ks@kll.com', '', '', '', 1),
(2, '', '', '', '', '', '', 19),
(3, '', '', '', '', '', '', 20),
(5, '', '', '', '', '', '', 22),
(6, '', '', '', '', '', '', 22),
(7, '', '', '', '', '', '', 24);

-- --------------------------------------------------------

--
-- Structure de la table `maison`
--

CREATE TABLE IF NOT EXISTS `maison` (
  `idmaison` varchar(25) NOT NULL,
  `idsm` int(20) NOT NULL,
  `idp` int(20) NOT NULL,
  `titre` varchar(150) NOT NULL,
  `descipt` text NOT NULL,
  `typappart` varchar(50) NOT NULL,
  `statu` varchar(100) NOT NULL,
  `chambres` int(20) NOT NULL,
  `douches` int(20) NOT NULL,
  `escalier` int(20) NOT NULL,
  `garages` int(20) NOT NULL,
  `surface` int(20) NOT NULL,
  `prix` int(50) NOT NULL,
  `prixd` int(50) NOT NULL,
  `prixr` int(50) NOT NULL,
  `cni` varchar(50) NOT NULL,
  `imgp` varchar(150) NOT NULL,
  `lits` int(20) NOT NULL,
  `pieces` int(20) NOT NULL,
  `m_stat` varchar(20) NOT NULL,
  PRIMARY KEY (`idmaison`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `maison`
--

INSERT INTO `maison` (`idmaison`, `idsm`, `idp`, `titre`, `descipt`, `typappart`, `statu`, `chambres`, `douches`, `escalier`, `garages`, `surface`, `prix`, `prixd`, `prixr`, `cni`, `imgp`, `lits`, `pieces`, `m_stat`) VALUES
('Mais12019', 1, 1, 'nouveau', 'dfla', 'Bureau', 'Location', 2, 4, 8, 2, 200, 120000, 1200000, 12546233, 'lmk5465', '5d34fa9635b37.png', 0, 0, 'autoris');

-- --------------------------------------------------------

--
-- Structure de la table `mdp_prop`
--

CREATE TABLE IF NOT EXISTS `mdp_prop` (
  `idmp` int(20) NOT NULL AUTO_INCREMENT,
  `mdp` varchar(100) NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`idmp`),
  KEY `idp` (`idp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `mdp_prop`
--

INSERT INTO `mdp_prop` (`idmp`, `mdp`, `idp`) VALUES
(1, 'ok', 1),
(6, 'ok', 10),
(7, 'ok', 11),
(18, 'ok', 22),
(19, 'ok', 22),
(20, 'ok', 24);

-- --------------------------------------------------------

--
-- Structure de la table `mp_user`
--

CREATE TABLE IF NOT EXISTS `mp_user` (
  `idmp` int(20) NOT NULL AUTO_INCREMENT,
  `idu` int(20) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  PRIMARY KEY (`idmp`),
  KEY `idu` (`idu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `propo_m`
--

CREATE TABLE IF NOT EXISTS `propo_m` (
  `idprop` int(20) NOT NULL AUTO_INCREMENT,
  `idmaison` varchar(25) NOT NULL,
  `balcon` varchar(4) NOT NULL,
  `animaux` varchar(4) NOT NULL,
  `barbeq` varchar(4) NOT NULL,
  `alarm` varchar(4) NOT NULL,
  `cuisinem` varchar(4) NOT NULL,
  `sauna` varchar(4) NOT NULL,
  `gym` varchar(4) NOT NULL,
  `ascens` varchar(4) NOT NULL,
  `sorti_u` varchar(4) NOT NULL,
  PRIMARY KEY (`idprop`),
  KEY `idmaison` (`idmaison`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `propo_m`
--

INSERT INTO `propo_m` (`idprop`, `idmaison`, `balcon`, `animaux`, `barbeq`, `alarm`, `cuisinem`, `sauna`, `gym`, `ascens`, `sorti_u`) VALUES
(7, '0', '', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non'),
(8, '0', '', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non'),
(9, '0', '', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non'),
(10, '0', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non', 'non'),
(11, '0', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non', 'non'),
(12, '0', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non', 'non'),
(13, '6', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'oui', 'non', 'non'),
(14, 'Mais62019', '', 'on', '', '', 'on', '', '', '', ''),
(15, 'Mais72019', '', 'on', '', '', 'on', '', '', 'on', ''),
(16, 'Mais12019', '', '', '', '', '', '', '', '', ''),
(17, 'Mais12019', '', 'on', '', '', 'on', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `proprio`
--

CREATE TABLE IF NOT EXISTS `proprio` (
  `idp` int(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(60) NOT NULL,
  `logins` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `proff` varchar(50) NOT NULL,
  `no_cni` varchar(25) NOT NULL,
  `p_photo` varchar(150) NOT NULL,
  `aprop` text NOT NULL,
  `statu` varchar(10) NOT NULL,
  PRIMARY KEY (`idp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `proprio`
--

INSERT INTO `proprio` (`idp`, `nom`, `prenom`, `logins`, `email`, `contact`, `proff`, `no_cni`, `p_photo`, `aprop`, `statu`) VALUES
(1, 'Kouassi', 'Mark', 'kkra', 'ks@gmail.com', '20 46 21 01', '', '', '5d34849038fd9.png', 'Eh oui', 'autoris'),
(6, 'Suini Olive', '', '', 'suiniolive8@gmail.com', '', '', '', '', '', 'autoris'),
(7, 'ok', '', '', 'pm@gmail.com', '', '', '', '', '', 'autoris'),
(22, 'lalan', '', '', 'lalan@gmail.com', '', '', '', '', '', ''),
(23, 'lalan', '', '', 'lalan@gmail.com', '', '', '', '', '', ''),
(24, 'ol', '', '', 'ol@gmail.com', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `p_messagerie`
--

CREATE TABLE IF NOT EXISTS `p_messagerie` (
  `idmp` int(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `tel` varchar(150) NOT NULL,
  `messages` text NOT NULL,
  `idmaison` int(20) NOT NULL,
  `idpe` int(20) NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`idmp`),
  KEY `idp` (`idp`),
  KEY `idpe` (`idpe`),
  KEY `idmaison` (`idmaison`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `titrep`
--

CREATE TABLE IF NOT EXISTS `titrep` (
  `idtu` int(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) NOT NULL,
  `idmaison` int(20) NOT NULL,
  PRIMARY KEY (`idtu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `idu` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `logins` varchar(50) NOT NULL,
  PRIMARY KEY (`idu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `video_m`
--

CREATE TABLE IF NOT EXISTS `video_m` (
  `idv` int(20) NOT NULL AUTO_INCREMENT,
  `video` varchar(150) NOT NULL,
  `idmaison` int(20) NOT NULL,
  PRIMARY KEY (`idv`),
  KEY `idp` (`idmaison`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `idville` int(30) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `sgeo` varchar(30) NOT NULL,
  `addr` varchar(30) NOT NULL,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`idville`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`idville`, `nom`, `sgeo`, `addr`, `img`) VALUES
(1, 'Abidjan', 'sud', '', ''),
(2, 'Bouake', 'centre', '', ''),
(3, 'Yamoussoukro', 'centre-sud', '', ''),
(4, 'Korhogo', 'nord', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
