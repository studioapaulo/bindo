-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 22 Juillet 2019 à 03:09
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bd_apl_sold`
--
CREATE DATABASE IF NOT EXISTS `bd_apl_sold` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_apl_sold`;

-- --------------------------------------------------------

--
-- Structure de la table `myaccount`
--

CREATE TABLE IF NOT EXISTS `myaccount` (
  `idc` int(20) NOT NULL AUTO_INCREMENT,
  `solde_actu` varchar(50) NOT NULL,
  `a_payer` varchar(50) NOT NULL,
  `a_payer_par_mois` varchar(50) NOT NULL,
  `idp` int(20) NOT NULL,
  PRIMARY KEY (`idc`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `myaccount`
--

INSERT INTO `myaccount` (`idc`, `solde_actu`, `a_payer`, `a_payer_par_mois`, `idp`) VALUES
(1, '10000', '0', '15000', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
